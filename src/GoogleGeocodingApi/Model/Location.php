<?php
namespace GoogleGeocodingApi\Model;

use Doctrine\ORM\Mapping as ORM;
use GoogleGeocodingApi\Traits\CommonTrait;

/**
 * CourierLocation
 *
 * @ORM\Table(name="gc_locations", indexes={@ORM\Index(name="hash", columns={"hash"}), @ORM\Index(name="latitude", columns={"latitude", "longitude"})})
 * @ORM\Entity
 */
class Location
{
    use CommonTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=20, scale=14, nullable=false)
     */
    public $latitude = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=20, scale=14, nullable=false)
     */
    public $longitude = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="postIndex", type="string", length=20, nullable=false)
     */
    public $postIndex = '';

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=100, nullable=false)
     */
    public $country = '';

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=100, nullable=false)
     */
    public $city = '';

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=500, nullable=false)
     */
    public $street = '';

    /**
     * @var string
     *
     * @ORM\Column(name="house", type="string", length=50, nullable=false)
     */
    public $house = '';

    /**
     * @var string
     *
     * @ORM\Column(name="apartment", type="string", length=50, nullable=false)
     */
    public $apartment = '';

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="integer", length=11, nullable=false)
     */
    public $hash = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    public $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getPostIndex()
    {
        return $this->postIndex;
    }

    /**
     * @param string $postIndex
     */
    public function setPostIndex($postIndex)
    {
        $this->postIndex = $postIndex;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param string $house
     */
    public function setHouse($house)
    {
        $this->house = $house;
    }

    /**
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * @param string $apartment
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

}