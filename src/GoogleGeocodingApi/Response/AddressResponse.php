<?php
namespace GoogleGeocodingApi\Response;

use GoogleGeocodingApi\Traits\CommonTrait;

class AddressResponse
{
    use CommonTrait;
    
    const COUNTRY = 'country';

    const POSTAL_CODE = 'postal_code';

    const LOCALITY = 'locality';

    const ROUTE = 'route';

    const STREET_NUMBER = 'street_number';

    const SUBPREMISE = 'subpremise';

    public $latitude = 0;

    public $longitude = 0;
    
    public $country = '';

    public $postIndex = '';

    public $city = '';

    public $street = '';

    public $house = '';

    public $apartment = '';

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return (bool) ($this->latitude && $this->longitude);
    }

    /**
     * @return bool
     */
    public function hasOnlyCountry() 
    {
        if ((! empty($this->country))
            && ($this->postIndex == '')
            && ($this->city == '')
            && ($this->street == '')
            && ($this->house == '')
            && ($this->apartment == '')) {
            
            return true;
        }
        
        return false;
    }

}