<?php
namespace GoogleGeocodingApi\Service;

use Doctrine\ORM\EntityManager;
use GoogleGeocodingApi\Request\AddressRequest;
use GoogleGeocodingApi\Response\AddressResponse;

class GeocodingService
{
    /**
     * @var GeocodingApiService|null
     */
    private $apiService = null;

    /**
     * @var CashInterface|null
     */
    private $cashService = null;

    /**
     * GeocodingService constructor.
     *
     * @param $url
     * @param $accessKey
     * @param CashInterface $cashService
     * @param array $params
     */
    public function __construct($url, $accessKey, array $params = array(), CashInterface $cashService = null)
    {
        $this->apiService = new GeocodingApiService($url, $accessKey, $params);
        $this->cashService = $cashService;
    }

    /**
     * @param $fullAddress
     *
     * @return bool|AddressResponse
     */
    public function getCoordinatesByFullAddress($fullAddress)
    {
        $request = new AddressRequest();
        $request->fullAddress = $fullAddress;

        return $this->getCoordinates($request);
    }

    /**
     *
     * @param AddressRequest $request
     *
     * @return bool|AddressResponse
     */
    public function getCoordinates(AddressRequest $request)
    {
        $response = new AddressResponse();

        if ($request->validate() == false) {

            return $response;
        }

        $cashResponse = $this->getResponseFromCash($request);
        if ($cashResponse instanceof AddressResponse) {

            return $cashResponse;
        }

        $locationResponse = $this->getApiService()->getLocation($request);
        if ($this->validate($locationResponse) == false) {

            return false;
        }

        $response->latitude = $locationResponse->results[0]->geometry->location->lat;
        $response->longitude = $locationResponse->results[0]->geometry->location->lng;
        foreach ($locationResponse->results[0]->address_components as $key => $component) {
            if (isset($component->types[0])) {
                if ($component->types[0] == AddressResponse::COUNTRY) {
                    $response->country = $component->long_name;
                } else                
                if ($component->types[0] == AddressResponse::POSTAL_CODE) {
                    $response->postIndex = $component->long_name;
                } else
                if ($component->types[0] == AddressResponse::LOCALITY) {
                    $response->city = $component->long_name;
                } else
                if ($component->types[0] == AddressResponse::ROUTE) {
                    $response->street = $component->long_name;
                } else
                if ($component->types[0] == AddressResponse::STREET_NUMBER) {
                    $response->house = $component->long_name;
                } else
                if ($component->types[0] == AddressResponse::SUBPREMISE) {
                    $response->apartment = $component->long_name;
                }
            }
        }

        $this->writeResponseToCash($response, $request->getHash());

        return $response;
    }

    /**
     * @param AddressRequest $request
     *
     * @return bool
     */
    private function getResponseFromCash(AddressRequest $request)
    {
        if ($this->getCashService() instanceof CashInterface) {

            return $this->getCashService()->getResponse($request);
        }

        return true;
    }

    /**
     * @param AddressResponse $response
     *
     * @param $hash
     *
     * @return bool
     */
    private function writeResponseToCash(AddressResponse $response, $hash)
    {
        if ($this->getCashService() instanceof CashInterface) {

            return $this->getCashService()->writeResponse($response, $hash);
        }

        return true;
    }

    /**
     * @param $locationResponse
     *
     * @return bool
     */
    private function validate($locationResponse)
    {
        if (empty($locationResponse)) {

            return false;
        }

        if ($locationResponse->status != "OK") {

            return false;
        }

        if ((count($locationResponse->results) == 0)) {

            return false;
        }

        if (! isset($locationResponse->results[0]->geometry)) {

            return false;
        }

        if (! isset($locationResponse->results[0]->geometry->location)) {

            return false;
        }

        if ((! isset($locationResponse->results[0]->geometry->location->lat)) ||
            (! isset($locationResponse->results[0]->geometry->location->lng))) {

            return false;
        }

        if (! isset($locationResponse->results[0]->address_components)) {

            return false;
        }

        return true;
    }

    /**
     * @return GeocodingApiService|null
     */
    public function getApiService()
    {
        return $this->apiService;
    }

    /**
     * @return CashInterface|null
     */
    public function getCashService()
    {
        return $this->cashService;
    }

}