<?php
namespace GoogleGeocodingApi\Service;

abstract class BaseGeoApiService
{
    private $params = array();

    private $accessKey = '';

    private $errorMessage = '';

    /**
     * BaseGeoApi constructor.
     * @param array $params
     * @param string $accessKey
     */
    public function __construct($accessKey, array $params)
    {
        $this->params = $params;
        $this->accessKey = $accessKey;
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        $params = $this->params;
        if (! empty($this->accessKey)) {
            $params['key'] = $this->accessKey;
        }

        return $params;
    }

    /**
     * @return array
     */
    protected function getHeaders()
    {
        $headers = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => true,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36 OPR/39.0.2256.48',
            CURLOPT_HTTPHEADER => array(
                'application/json',
            )
        );

        return $headers;
    }

    /**
     * @param $jsonResponse
     *
     * @return bool|mixed|null
     */
    protected function getResponseAsObject($jsonResponse)
    {
        if ($jsonResponse === '') {
            $this->setErrorMessage('Пустой ответ.');

            return null;
        }

        if ($jsonResponse === false) {
            $this->setErrorMessage('Не могу получить ответ на запрос.');

            return null;
        }

        $response = json_decode($jsonResponse);
        if (isset($response->errors) && (count($response->errors) > 0)) {
            $this->setErrorMessage($response->errors[0]);
            $response = null;
        }

        if ($response === false) {

            $response = null;
        }

        return $response;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

}