<?php
namespace GoogleGeocodingApi\Service;

use GoogleGeocodingApi\Request\AddressRequest;

class GeocodingApiService extends BaseGeoApiService
{
    private $url = '';

    private $curlService = null;

    /**
     * GeocodingService constructor.
     *
     * @param string $url
     * @param string $accessKey
     * @param $params
     */
    public function __construct($url, $accessKey, $params)
    {
        parent::__construct($accessKey, $params);
        $this->url = $url;
        $this->curlService = new CurlService();
    }

    /**
     * @param AddressRequest $request
     *
     * @return bool|mixed|null
     */
    public function getLocation(AddressRequest $request)
    {
        $response = $this->getCurlService()
            ->setOptions($this->getHeaders())
            ->get(
                $this->url,
                array_merge(
                    $this->getOptions(),
                    array('address' => $request->getFormattedAddress())
                )
            );

        return $this->getResponseAsObject($response);
    }

    /**
     * @return CurlService|null
     */
    public function getCurlService()
    {
        return $this->curlService;
    }

}