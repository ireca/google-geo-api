<?php
namespace GoogleGeocodingApi\Service;

use Doctrine\ORM\EntityManager;
use GoogleGeocodingApi\Model\Location;
use GoogleGeocodingApi\Request\AddressRequest;
use GoogleGeocodingApi\Response\AddressResponse;

class MySQLCashService implements CashInterface
{
    /**
     * @var EntityManager
     */
    private $em = null;

    /**
     * MySQLCashService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param AddressRequest $request
     *
     * @return mixed|void
     */
    public function getResponse(AddressRequest $request)
    {
        $response = new AddressResponse();

        /** @var \GoogleGeocodingApi\Model\Location $location */
        $location = $this->getEm()->getRepository('GoogleGeocodingApi\\Model\\Location')->findOneBy(array('hash' => $request->getHash()));
        if (! is_null($location)) {
            $response->setAttributes(get_object_vars($location));
            
            return $response;
        }

        return null;
    }

    /**
     * @param AddressResponse $response
     *
     * @return bool|mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function writeResponse(AddressResponse $response, $hash)
    {
        if (($response->latitude == 0) || ($response->longitude == 0)) {

            return true;
        }

        $location = new Location();
        $location->setAttributes($response->getAttributes());
        $location->hash = $hash;
        $location->createdAt = new \DateTime();
        $this->getEm()->persist($location);
        $this->getEm()->flush();

        return true;
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

}