<?php
namespace GoogleGeocodingApi\Service;

use GoogleGeocodingApi\Request\AddressRequest;
use GoogleGeocodingApi\Response\AddressResponse;

interface CashInterface
{
    /**
     * @param AddressRequest $request
     *
     * @return mixed
     */
    public function getResponse(AddressRequest $request);

    /**
     * @param AddressResponse $response
     *
     * @return mixed
     */
    public function writeResponse(AddressResponse $response, $hash);
}