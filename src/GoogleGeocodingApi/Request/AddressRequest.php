<?php
namespace GoogleGeocodingApi\Request;

use GoogleGeocodingApi\Traits\CommonTrait;

class AddressRequest
{
    use CommonTrait;

    public $fullAddress = '';

    public $country = '';

    public $city = '';

    public $street = '';

    public $house = '';

    public $apartment = '';

    /**
     * @return string
     */
    public function getFormattedAddress()
    {
        $formattedAddress = array();

        if (! empty($this->fullAddress)) {

            return $this->fullAddress;
        }

        if (! empty($this->city)) {
            $formattedAddress[] = $this->city;
        }

        if (! empty($this->street)) {
            $formattedAddress[] = $this->street;
        }

        if (! empty($this->house)) {
            $formattedAddress[] = $this->house;
        }

        if (! empty($this->apartment)) {
            $formattedAddress[] = $this->apartment;
        }


        return implode('+', $formattedAddress);
    }

    /**
     * @return int
     */
    public function getHash()
    {
        if (! empty($this->fullAddress)) {

            return crc32(str_replace(' ', '', $this->fullAddress));
        }

        return crc32(str_replace(' ', '', $this->country . $this->city . $this->street . $this->house));
    }

    /**
     * @return bool
     */
    public function validate()
    {
        if (! empty($this->fullAddress)) {
            $address = $this->fullAddress;
        } else {
            $address = $this->street . $this->city;
        }

        if (empty($address) || strlen($address) < 10) {

            return false;
        }

        return true;
    }

}