<?php
namespace GoogleGeocodingApi\Factory;

use GoogleGeocodingApi\Service\EntityManagerService;
use GoogleGeocodingApi\Service\GeocodingService;
use GoogleGeocodingApi\Service\MySQLCashService;

class GeocodingServiceFactory
{
    /**
     * @param $url
     * @param $accessKey
     * @param $params
     * @param $cashConfig
     *
     * @return GeocodingService
     * @throws \Doctrine\ORM\ORMException
     */
    public static function create($url, $accessKey, array $params = array(), array $cashConfig = array())
    {
        return new GeocodingService(
            $url,
            $accessKey,
            $params,
            self::getCashService($cashConfig)
        );
    }    
    
    /**
     * @param $config
     *
     * @return GeocodingService
     * @throws \Doctrine\ORM\ORMException
     */
    public static function createByConfig($config)
    {
        return new GeocodingService(
            $config['googleGeo']['url'],
            $config['googleGeo']['accessKey'],
            $config['googleGeo']['params'],
            self::getCashService(
                isset($config['googleGeo']['cash']) ? $config['googleGeo']['cash'] : array()
            )
        );
    }

    /**
     * @param $cashConfig
     *
     * @return MySQLCashService|null
     * @throws \Doctrine\ORM\ORMException
     */
    private static function getCashService(array $cashConfig)
    {
        $cashService = null;
        if (count($cashConfig) == 0) {

            return  $cashService;
        }

        if ($cashConfig['type'] == 'mysql') {
            $emService = new EntityManagerService($cashConfig['databaseConfig']);
            $cashService = new MySQLCashService($emService->getEm());
        }

        return $cashService;
    }

}