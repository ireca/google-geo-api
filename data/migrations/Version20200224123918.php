<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200224123918 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery("
            CREATE TABLE `gc_locations` (
              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              `latitude` decimal(20,14) NOT NULL DEFAULT '0.00000000000000',
              `longitude` decimal(20,14) NOT NULL DEFAULT '0.00000000000000',
              `postIndex` varchar(20) NOT NULL DEFAULT '',
              `country` varchar(100) NOT NULL DEFAULT '',
              `city` varchar(100) NOT NULL DEFAULT '',
              `street` varchar(500) NOT NULL DEFAULT '',
              `house` varchar(50) NOT NULL DEFAULT '',
              `apartment` varchar(50) NOT NULL DEFAULT '',
              `hash` bigint(14) NOT NULL,
              `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              UNIQUE KEY `hash` (`hash`),
              KEY `latitude` (`latitude`,`longitude`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('`gc_locations`');
    }
}
