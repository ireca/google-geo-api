# Google Geo API - Gets coordinates of an address with using [Google Geocoding API]((https://developers.google.com/maps/documentation/geocoding/start?hl=ru))

Google Geo API - is a library that allows you to get coordinates at by address using the [Google Geocoding API](https://developers.google.com/maps/documentation/geocoding/start?hl=ru). 
In addition, you can enable caching, which will significantly reduce the use of the [Google Geocoding API](https://developers.google.com/maps/documentation/geocoding/start?hl=ru).

## Installation

Install the latest version with

```bash
$ composer require antonioo83/google-geo-api
$ php vendor/doctrine/migrations/bin/doctrine-migrations.php migrations:migrate --configuration config/migrations-config.yml --db-configuration config/database-config.php
```

## Basic Usage

```php
<?php

use GoogleGeocodingApi\Factory\GeocodingServiceFactory;
use GoogleGeocodingApi\Request\AddressRequest;

$config = array(
    'googleGeo' => array(
        // set "cash" parameter as empty array if don't want using cashe. 
        'cash' => array(
            'type' => 'mysql',
            'databaseConfig' => array(
                'driver'   => 'pdo_mysql',
                'user'     => 'root',
                'password' => '',
                'dbname'   => 'geocoding',
                'charset' => 'UTF8',
            )
        ),
        'url' => 'https://maps.googleapis.com/maps/api/geocode/json',
        'accessKey' => 'Your private access key',
        'params' => array(
            'language' => 'ru',
            'result_type' => '',
            'locationType' => '',
        )
    )
);
$service = GeocodingServiceFactory::createByConfig($config);
$result = $service->getCoordinatesByFullAddress('Russia, St. Petersburg, Nevsky pr., 92');
// or
$request = new AddressRequest();
$request->country = 'Russia';
$request->city = 'St.Petersburg';
$request->street = 'Nevsky pr.';
$request->house = '92';
$service = GeocodingServiceFactory::createByConfig($config);
$result = $service->getCoordinates($request);
var_dump($result);

// After that you will see next result
/*
object(GoogleGeocodingApi\Response\AddressResponse)[401]
  public 'latitude' => float 59.9326403
  public 'longitude' => float 30.3530359
  public 'country' => string 'Russia' (length=6)
  public 'postIndex' => string '191025' (length=6)
  public 'city' => string 'Sankt-Peterburg' (length=15)
  public 'street' => string 'Nevsky Avenue' (length=13)
  public 'house' => string '92' (length=2)
  public 'apartment' => string '' (length=0)
*/
```

## Documentation

- [Google Geocoding API](https://developers.google.com/maps/documentation/geocoding/start?hl=ru)
- [Google Cloud Platform](https://cloud.google.com/)
- [Doctrine](https://www.doctrine-project.org/)

### Requirements

- Google Geo API works with PHP 5.4 or above.

### Submitting bugs and feature requests

Bugs and feature request are tracked on [GitLab](https://gitlab.com/dashboard/issues?assignee_username=antonioo83)

### Framework Integrations

- Frameworks and libraries using [PSR-4](https://www.php-fig.org/psr/psr-4/)
  can be used very easily with Google Geo API.
- [Symfony](http://symfony.com);
- [Zend](https://framework.zend.com/);
- [Yii](https://www.yiiframework.com/) and other.

### Author

Anton Yurchenko - <antonioo83@mail.ru>

### License

Google Geo API is licensed under the MIT License - see the `LICENSE` file for details


