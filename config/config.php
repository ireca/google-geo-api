<?php
return array(
    'cash' => array(
        'type' => 'mysql',
        'databaseConfig' => include (__DIR__ . '/database-config.php')
    ),
    'googleGeo' => array(
        'url' => 'https://maps.googleapis.com/maps/api/geocode/json',
        'accessKey' => '',
        'params' => array(
            'language' => 'ru',
            /**
             * Return type.
             *
             * country|street_address|postal_code
             *
             * Full list of types: https://developers.google.com/maps/documentation/geocoding/intro#Types
             *
             * @var string
             */
            'result_type' => '',

            /**
             * Location accuracy.
             *
             * "ROOFTOP", "RANGE_INTERPOLATED", "GEOMETRIC_CENTER", "APPROXIMATE"
             *
             * @var string
             */
            'locationType' => '',
        )
    )
);